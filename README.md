# Scripts for building GPAW

This project is a collection of scripts that I use for building GPAW and
required dependencies on different environments:
* [mahti](https://docs.csc.fi/computing/systems-mahti/)
* [puhti](https://docs.csc.fi/computing/systems-puhti/)
* [triton](https://scicomp.aalto.fi/triton/)
* [ubuntu](https://ubuntu.com) (tested on version 22.04)

Scripts are available also for these systems but they are not updated:
* [vera](https://www.c3se.chalmers.se/about/Vera)

Note that the build instructions here are intended for development use.
The HPC systems might provide builds of stable GPAW versions.


## Usage for one of the listed environments

These instructions assume that the bash shell is used,
the software is built under `$GPAW_USERAPPL` and
`$SYSTEM` is the system at hand (for example `triton`).

1. Define environment variables:

       export GPAW_USERAPPL=$HOME/appl
       export SYSTEM=triton  # change this to the correct one

1. Clone this repository:

       git clone https://gitlab.com/trossi/gpaw-build-scripts.git $GPAW_USERAPPL/gpaw-build-scripts
       # Alternatively with SSH:
       # git clone git@gitlab.com:trossi/gpaw-build-scripts.git $GPAW_USERAPPL/gpaw-build-scripts

1. Set up the modules:

       module use $GPAW_USERAPPL/gpaw-build-scripts/modules/$SYSTEM

1. To avoid repeating this set up on every login,
   include the following lines in `~/.bashrc`:

       export GPAW_USERAPPL=$HOME/appl
       module use $GPAW_USERAPPL/gpaw-build-scripts/modules/$SYSTEM  # replace $SYSTEM here with the actual value

1. Install GPAW and dependencies
   1. [ASE](https://gitlab.com/ase/ase):

          mkdir -p $GPAW_USERAPPL/ase
          cd $GPAW_USERAPPL/ase
          git clone -b 3.22.1 https://gitlab.com/ase/ase.git 3.22.1
          # Alternatively with SSH:
          # git clone -b 3.22.1 git@gitlab.com:ase/ase.git 3.22.1

   1. [libxc](https://gitlab.com/libxc/libxc) (if not preinstalled in the system):

          mkdir -p $GPAW_USERAPPL/libxc
          cd $GPAW_USERAPPL/libxc
          git clone -b 4.3.4 https://gitlab.com/libxc/libxc.git 4.3.4
          # Build
          ln -s $GPAW_USERAPPL/gpaw-build-scripts/build/libxc/build-$SYSTEM.sh
          ./build-$SYSTEM.sh 4.3.4

   1. [GPAW](https://gitlab.com/gpaw/gpaw)

          mkdir -p $GPAW_USERAPPL/gpaw
          cd $GPAW_USERAPPL/gpaw
          git clone -b 22.1.0 https://gitlab.com/gpaw/gpaw.git 22.1.0
          # Alternatively with SSH:
          # git clone -b 22.1.0 git@gitlab.com:gpaw/gpaw.git 22.1.0
          # Build
          ln -s $GPAW_USERAPPL/gpaw-build-scripts/build/gpaw/build-$SYSTEM.sh
          ln -s $GPAW_USERAPPL/gpaw-build-scripts/build/gpaw/siteconfig-$SYSTEM.py
          ./build-$SYSTEM.sh 22.1.0

   1. PAW setups for GPAW:

          mkdir -p $GPAW_USERAPPL/gpaw-setups
          cd $GPAW_USERAPPL/gpaw-setups
          # Download
          $GPAW_USERAPPL/gpaw-build-scripts/build/gpaw-setups/install.sh

   1. Test the installation:

          module load gpaw/22.1.0
          gpaw info
          gpaw test
          # Run the test suite
          pytest --pyargs gpaw -v -n 8
          mpirun -n 2 gpaw python -m pytest --pyargs gpaw -v
          mpirun -n 4 gpaw python -m pytest --pyargs gpaw -v
          mpirun -n 8 gpaw python -m pytest --pyargs gpaw -v

      See information about pytests [here](https://wiki.fysik.dtu.dk/gpaw/devel/testing.html).

1. The [example submit scripts](submit) can be used, for example, as:

       sbatch -J test -N 1 -t 0:10:0 submit.sh test.py


## Setting up a new environment

The build scripts here can be used as a starting point for building gpaw on
other environments. In the following, `$SYSTEM` is the new system at hand.

1. The scripts assume that software is built in `$GPAW_USERAPPL`:

       export GPAW_USERAPPL=$HOME/appl

1. Clone this repository:

       git clone https://gitlab.com/trossi/gpaw-build-scripts.git $GPAW_USERAPPL/gpaw-build-scripts

1. Identify the used module file flavor by looking into some existing module:

       module show some_existing_module

   Compare the syntax, for example, to
   [a lua file for vera](modules/vera/gpaw/dev.lua) and
   [a tcl file for tegner](modules/tegner/gpaw/dev).

1. Use existing module files with correct syntax as a template.
   For example, using lua files for vera:

       cp -r $GPAW_USERAPPL/gpaw-build-scripts/modules/vera $GPAW_USERAPPL/gpaw-build-scripts/modules/$SYSTEM

   or tcl files for tegner:

       cp -r $GPAW_USERAPPL/gpaw-build-scripts/modules/tegner $GPAW_USERAPPL/gpaw-build-scripts/modules/$SYSTEM

1. Use existing build scripts as a template. The build scripts can require
   tuning system-by-system, but the scripts for vera are often a good starting
   point for building with MKL:

       cp $GPAW_USERAPPL/gpaw-build-scripts/build/gpaw/build-vera.sh $GPAW_USERAPPL/gpaw-build-scripts/build/gpaw/build-${SYSTEM}.sh
       cp $GPAW_USERAPPL/gpaw-build-scripts/build/gpaw/siteconfig-vera.py $GPAW_USERAPPL/gpaw-build-scripts/build/gpaw/siteconfig-${SYSTEM}.py

1. Edit all the module files and build scripts created for `$SYSTEM` so that
   they have correct `module load`'s (e.g., python3 and MKL) and file paths
   for the system in question.

1. Then, proceed and test with the installation instructions above.
