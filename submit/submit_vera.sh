#!/bin/bash
#SBATCH --cpus-per-task=2  # Do not use hyperthreading

export START_TIME=`date +%s.%3N`
export TIME_LIMIT=`squeue -h -o %l -j $SLURM_JOB_ID`

module purge
module load gpaw/dev

echo "$*"
mpirun gpaw python $*
