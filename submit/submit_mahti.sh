#!/bin/bash
#SBATCH --ntasks-per-node=128
#SBATCH --cpus-per-task=1

export START_TIME=`date +%s.%3N`
export TIME_LIMIT=`squeue -h -o %l -j $SLURM_JOB_ID`

module load gpaw/dev
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

echo "$*"
srun gpaw python $*
