#!/bin/bash
#SBATCH --constraint=Broadwell
#SBATCH --ntasks-per-node=36
#SBATCH --mem=0

export START_TIME=`date +%s.%3N`
export TIME_LIMIT=`squeue -h -o %l -j $SLURM_JOB_ID`

module load gpaw/dev

echo "$*"
srun gpaw python $*
