#!/bin/bash
#SBATCH -p 2TB
#SBATCH --constraint=IvyBridge
#SBATCH --ntasks-per-node=48

export START_TIME=`date +%s.%3N`
export TIME_LIMIT=`squeue -h -o %l -j $SLURM_JOB_ID`

module load gpaw/dev

echo "$*"
mpirun gpaw python $*
