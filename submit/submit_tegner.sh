#!/bin/bash
#SBATCH --constraint=Haswell
#SBATCH --ntasks-per-node=24

export START_TIME=`date +%s.%3N`
export TIME_LIMIT=`squeue -h -o %l -j $SLURM_JOB_ID`

module load gpaw/dev

echo "$*"
mpirun gpaw python $*
