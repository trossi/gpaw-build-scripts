#!/bin/bash
#SBATCH -t 02:00:00
#SBATCH --mem-per-cpu=2G
#SBATCH -N 1
#SBATCH --ntasks-per-node=8
#SBATCH -J pytest
#SBATCH -o pytest-%j.out

version=${1:-dev}

echo "ver=$version n=$SLURM_NTASKS_PER_NODE"

module purge
module load gpaw/$version
module list
gpaw info

cd $GPAWHOME/gpaw/test
srun pytest --basetemp=$WRKDIR/tmp/pytest-$SLURM_JOBID -v

# Run this script like
# for version in dev; do for n in 1 2 4 8; do sbatch -o pytest-$version-$n.out -J pytest-$version-$n --ntasks-per-node=$n pytest.sh $version; done; done
