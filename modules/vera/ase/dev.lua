whatis("ASE")

conflict("ASE")

local version = myModuleVersion()
local root = pathJoin(os.getenv("GPAW_USERAPPL"), myModuleName(), version)

load("iccifort/2019.5.281")
load("impi/2018.5.288")
load("imkl/2019.5.281")
load("Python/3.7.4")
load("Tkinter/3.7.4")
load("matplotlib/3.1.1-Python-3.7.4")

prepend_path("PYTHONPATH", root)
prepend_path("PATH", pathJoin(root, "tools"))
prepend_path("PATH", pathJoin(root, "bin"))
