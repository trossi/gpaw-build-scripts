whatis("GPAW")

conflict("GPAW")

local version = myModuleVersion()
local root = pathJoin(os.getenv("GPAW_USERAPPL"), myModuleName(), version)

load(pathJoin("ase", version))
load("libxc/4.3.4")
load("gpaw-setups/0.9.20000")

prepend_path("PYTHONPATH", root)
prepend_path("PYTHONPATH", pathJoin(root, "build/lib.linux-x86_64-3.7"))
prepend_path("PATH", pathJoin(root, "tools"))
