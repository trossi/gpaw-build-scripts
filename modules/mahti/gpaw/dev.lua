local name = myModuleName()
local version = myModuleVersion():match('[^-]*')
local build = "cpu"
if myModuleVersion():match('-') then
  build = myModuleVersion():match('-.*'):sub(2)
end
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

load("gcc/11.2.0")

local python_root, python_local_root
if build == "cpu" then
  python_root = pathJoin("/appl/spack/v017/views/gpaw-python3.9")
  python_local_root = pathJoin(os.getenv("GPAW_USERAPPL"), "gpaw-python", "3.9", "local")
  load("openmpi/4.1.2")
  load("fftw/3.3.10-mpi-omp")
elseif build == "cuda" then
  python_root = pathJoin("/appl/spack/v017/views/gpaw-python3.9-cuda11.5")
  python_local_root = pathJoin(os.getenv("GPAW_USERAPPL"), "gpaw-python", "3.9-cuda11.5", "local")
  load("openmpi/4.1.2-cuda")
  -- load("fftw/3.3.10-mpi-omp")
  load("cuda/11.5.0")
else
  LmodError("unknown build: " .. build)
end

load("openblas/0.3.18-omp")
load("netlib-scalapack/2.1.0")

load(pathJoin("ase", version))
load("gpaw-setups/0.9.20000")


-- Python
setenv("PYTHONHOME", python_root)
prepend_path("PATH", pathJoin(python_root, "bin"))
prepend_path("PYTHONPATH", pathJoin(python_root, "lib", "python3.9", "site-packages"))
prepend_path("LD_LIBRARY_PATH", pathJoin(python_root, "lib"))
setenv("MPLBACKEND","TkAgg")
setenv("PYTHONUSERBASE", python_local_root)
prepend_path("PATH", pathJoin(python_local_root, "bin"))

-- GPAW
setenv("GPAWHOME", root)
prepend_path("PYTHONPATH", root)
prepend_path("PYTHONPATH", pathJoin(root, "build-" .. build, "lib"))
prepend_path("PATH", pathJoin(root, "tools"))
prepend_path("PATH", pathJoin(root, "build-" .. build, "bin"))

-- env
setenv("UCX_LOG_LEVEL", "ERROR")
setenv("OMP_NUM_THREADS", 1)
