local version = myModuleVersion()
local root = pathJoin("/appl/spack/v018/views/gpaw-python")
local local_root = pathJoin(os.getenv("GPAW_USERAPPL"), myModuleName(), version, "local")

-- Load python from root
setenv("PYTHONHOME", root)
prepend_path("PATH", pathJoin(root, "bin"))
prepend_path("PYTHONPATH", pathJoin(root, "lib/python3.9/site-packages"))
prepend_path("LD_LIBRARY_PATH", pathJoin(root, "lib"))
setenv("MPLBACKEND","TkAgg")

-- Add local python packages from pip
setenv("PYTHONUSERBASE", local_root)
prepend_path("PATH", pathJoin(local_root, "bin"))
