local name = myModuleName()
local version = myModuleVersion()
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

load("python/3.9.12")
load("gcc/11.3.0")
load("openmpi/4.1.4")
load("intel-oneapi-mkl/2022.1.0")
load("fftw/3.3.10-mpi-omp")

load("ase/3.21.0")
load("gpaw-setups/0.9.20000")

setenv("GPAWHOME", root)
setenv("OMP_NUM_THREADS", 1)

prepend_path("PYTHONPATH", root)
prepend_path("PYTHONPATH", pathJoin(root, "build", "lib.linux-x86_64-3.9"))
prepend_path("PATH", pathJoin(root, "tools"))
