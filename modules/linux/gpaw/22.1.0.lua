local name = myModuleName()
local version = myModuleVersion()
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

load("ase/3.22.1")
load("gpaw-setups/0.9.20000")

local python_version = capture("python3 --version"):match('Python (%d+%.%d+)')
local platform = "linux-x86_64-" .. python_version

setenv("GPAW_HOME", root)
setenv("OMP_NUM_THREADS", 1)
prepend_path("PYTHONPATH", root)
prepend_path("PYTHONPATH", pathJoin(root, "build/lib." .. platform))
prepend_path("PATH", pathJoin(root, "tools"))
prepend_path("PATH", pathJoin(root, "build/bin." .. platform))
execute {cmd='complete -o default -C "python3 ' .. root .. '/gpaw/cli/complete.py" gpaw', modeA={"load"}}
