local name = myModuleName()
local version = myModuleVersion()
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

setenv("ASE_HOME", root)
prepend_path("PYTHONPATH", root)
prepend_path("PATH", pathJoin(root, "tools"))
prepend_path("PATH", pathJoin(root, "bin"))
