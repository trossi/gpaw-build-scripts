conflict("gpaw-setups")

local version = myModuleVersion()
local root = pathJoin(os.getenv("GPAW_USERAPPL"), myModuleName(), version)

append_path("GPAW_SETUP_PATH", pathJoin(root, "gpaw-setups-"..version))
append_path("GPAW_SETUP_PATH", pathJoin(root, "gpaw-basis-pvalence-"..version))
