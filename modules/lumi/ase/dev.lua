local version = myModuleVersion()
local root = pathJoin(os.getenv("GPAW_USERAPPL"), myModuleName(), version)

prepend_path("PYTHONPATH", root)
prepend_path("PATH", pathJoin(root, "tools"))
prepend_path("PATH", pathJoin(root, "bin"))
