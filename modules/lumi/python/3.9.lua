local version = myModuleVersion()
local local_root = pathJoin(os.getenv("GPAW_USERAPPL"), myModuleName(), version, "local")

load("LUMI/22.08")
load("cray-python/3.9.13.1")

-- Add local python packages from pip
setenv("PYTHONUSERBASE", local_root)
prepend_path("PATH", pathJoin(local_root, "bin"))
