local name = myModuleName()
local version = myModuleVersion():match('[^-]*')
local build = "cpu"
if myModuleVersion():match('-') then
  build = myModuleVersion():match('-.*'):sub(2)
end
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

load("python/3.9")
load("PrgEnv-gnu/8.3.3")
load("cray-fftw/3.3.10.1")
if build == "cpu" then

else
  if build == "hip" then
    load("cupy/11.6.0")
  elseif build == "hip-cupy1160-rocm533" then
    load("cupy/11.6.0-rocm533")
  else
    LmodError("unknown build: " .. build)
  end
  -- enable GPU-aware MPI (needed runtime)
  setenv("MPICH_GPU_SUPPORT_ENABLED", 1)
end

load(pathJoin("ase", version))
load("gpaw-setups/0.9.20000")

-- GPAW
setenv("GPAWHOME", root)
prepend_path("PYTHONPATH", root)
prepend_path("PYTHONPATH", pathJoin(root, "build-" .. build, "lib"))
prepend_path("PATH", pathJoin(root, "tools"))
prepend_path("PATH", pathJoin(root, "build-" .. build, "bin"))

-- env
setenv("OMP_NUM_THREADS", 1)
