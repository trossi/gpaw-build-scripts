local name = myModuleName()
local version = myModuleVersion():match('[^-]*')
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

load("LUMI/22.08")
load("cray-python/3.9.13.1")
load("PrgEnv-gnu/8.3.3")
load("cray-fftw/3.3.10.1")
load("gpaw-setups/0.9.20000")

-- GPAW
setenv("GPAWHOME", root)
prepend_path("PYTHONPATH", pathJoin(root, "build", "target"))
prepend_path("PATH", pathJoin(root, "build", "target", "bin"))

-- env
setenv("OMP_NUM_THREADS", 1)
