local name = myModuleName()
local version = myModuleVersion():match('[^-]*')
local build = "def"
if myModuleVersion():match('-') then
  build = myModuleVersion():match('-.*'):sub(2)
end
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

load("python/3.9")
load("PrgEnv-gnu/8.3.3")
load("craype-accel-amd-gfx90a")
if build == "def" then
  load("rocm/5.2.3")
elseif build == "rocm533" then
  load("rocm/5.3.3")
else
  LmodError("unknown build: " .. build)
end

setenv("CUPY_INSTALL_USE_HIP", 1)
setenv("HCC_AMDGPU_TARGET", "gfx90a")

prepend_path("PYTHONPATH", pathJoin(root .. "-" .. build, "build", "target"))

-- set ROCM_HOME to ROCM_PATH
execute {cmd="export ROCM_HOME=$ROCM_PATH", modeA={"load"}}
