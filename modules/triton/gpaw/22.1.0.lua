local name = myModuleName()
local version = myModuleVersion()
local platform = "linux-x86_64-3.8"
local root = pathJoin(os.getenv("GPAW_USERAPPL"), name, version)

load("python/3.8.7")
load("py-setuptools/58.2.0-python3")
load("py-virtualenv/16.7.6-python3")
-- load("py-scipy/...-python3")  -- scipy not available as a module for python/3.8.7
load("py-matplotlib/3.4.3-python3")
load("openmpi/4.0.5")
load("openblas/0.3.13-openmp")
load("netlib-scalapack/2.1.0-openmpi-openmp")
load("libxc/5.1.5")
load("fftw/3.3.10-openmpi-openmp")

load("ase/3.22.1")
load("gpaw-setups/0.9.20000")

setenv("GPAWHOME", root)
setenv("OMP_NUM_THREADS", 1)
prepend_path("PYTHONPATH", root)
prepend_path("PYTHONPATH", pathJoin(root, "build/lib." .. platform))
prepend_path("PATH", pathJoin(root, "tools"))
