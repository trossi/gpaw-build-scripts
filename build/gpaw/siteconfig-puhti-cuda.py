import os

mpicompiler = 'mpicc'
libraries = []
library_dirs = []
include_dirs = []
extra_compile_args = [
    #'-std=c99',
    '-O3',
    '-march=native',
    '-funroll-loops',
    '-fopenmp',
    '-fPIC',
    '-Wall',
    # '-Wextra',
    #'-DNDEBUG',
    '-g',
    ]
extra_link_args = ['-fopenmp']

# MPI
libraries += ['mpi', 'gomp']

# cuda
cuda = True
gpu_compiler = 'nvcc'
gpu_compile_args = ['-g', '-O3', '-gencode arch=compute_70,code=sm_70']
# dpath = os.environ['CUDA_INSTALL_ROOT']
dpath = os.environ['CUDA_HOME']
gpu_include_dirs += [f'{dpath}/include']
library_dirs += [f'{dpath}/lib64']
libraries += ['cudart', 'cublas', 'stdc++']

# Intel MKL
libraries += ['mkl_intel_lp64', 'mkl_core', 'mkl_sequential']

# fftw
fftw = True
libraries += ['fftw3']

# scalapack
scalapack = True
libraries += ['mkl_scalapack_lp64']
libraries += ['mkl_blacs_openmpi_lp64']

# libxc
libraries += ['xc']
dpath = '/appl/spack/v018/install-tree/gcc-11.3.0/libxc-5.1.7-4aszho'
include_dirs += [f'{dpath}/include']
library_dirs += [f'{dpath}/lib']
extra_link_args += [f'-Wl,-rpath,{dpath}/lib']

define_macros += [('GPAW_NO_UNDERSCORE_CBLACS', 1)]
define_macros += [('GPAW_NO_UNDERSCORE_CSCALAPACK', 1)]
define_macros += [('GPAW_ASYNC', 1)]
define_macros += [('GPAW_MPI2', 1)]
