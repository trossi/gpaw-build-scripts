compiler = 'cc -D_Float128=__float128'
mpicompiler = None
mpilinker = None
extra_compile_args = ['-std=c99', '-O3', '-fPIC', '-xHost']

scalapack = False
fftw = False

# libxc
libraries += ['xc']
dpath = '/pdc/vol/libxc/4.3.4'
include_dirs += [f'{dpath}/include']
library_dirs += [f'{dpath}/lib']
extra_link_args += [f'-Wl,-rpath,{dpath}/lib']
