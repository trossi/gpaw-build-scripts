#!/bin/bash

version=$1

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

module purge
module load ase/$version  # This loads MKL
module load libxc/4.3.4

cd $version
rm -rf build
GPAW_CONFIG=../siteconfig-vera.py python3 setup.py --remove-default-flags build_ext 2>&1 | tee build.log
