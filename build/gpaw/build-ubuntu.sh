#!/bin/bash

version=$1

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

module purge
module load gpaw/$version

cd $version
rm -rf build

GPAW_CONFIG=../siteconfig-ubuntu.py python3 setup.py build_ext 2>&1 | tee compile.log
