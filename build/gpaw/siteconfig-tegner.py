import os

mpicompiler = 'mpiicc'
libraries = []
library_dirs = []
include_dirs = []

# Intel MKL
libraries += ['mkl_sequential', 'mkl_core', 'mkl_rt']

# fftw
fftw = True
libraries += ['fftw3']
dpath = '/pdc/vol/fftw/3.3.7/gcc/7.2.0/openmpi/3.0/double'
include_dirs += [f'{dpath}/include']
library_dirs += [f'{dpath}/lib']
extra_link_args += [f'-Wl,-rpath,{dpath}/lib']

# scalapack
scalapack = True
libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# libxc
libraries += ['xc']
dpath = os.environ['GPAW_USERAPPL'] + '/libxc/4.3.4/xc'
include_dirs += [f'{dpath}/include']
library_dirs += [f'{dpath}/lib']
extra_link_args += [f'-Wl,-rpath,{dpath}/lib']

extra_compile_args = [
    #'-w',                      # disable all errors
    #'-xHost',                  # use host instruction set
    '-xAVX',                    # use AVX
    '-axCORE-AVX2,CORE-AVX-I',  # use optionally AVX2
    '-O3',
    '-ipo',
    '-funroll-all-loops',
    '-fPIC',
    '-std=c99',
    '-Wall',
    '-Wextra',
    '-DNDEBUG',
    '-fno-math-errno',
    ]
