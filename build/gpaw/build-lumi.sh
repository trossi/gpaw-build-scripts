#!/bin/bash

version=$1
build=${2:-cpu}

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

echo "Building $version for $build"
echo

module purge 2> /dev/null
module load gpaw/$version-$build

if [[ "$build" == "cpu" ]]; then
    if ! module list 2>&1 | grep -q "partition/C"; then
        echo "ERROR: partition/C not loaded"
        exit 1
    fi
elif [[ "$build" == "hip" ]]; then
    if ! module list 2>&1 | grep -q "partition/G"; then
        echo "ERROR: partition/G not loaded"
        exit 1
    fi

    which hipcc
    echo "HCC_AMDGPU_TARGET=$HCC_AMDGPU_TARGET"
fi

if module list 2>&1 | grep -q "partition/L"; then
    echo "ERROR: partition/L loaded"
    exit 1
fi

cd $version
BUILD_DPATH=build-$build
rm -rf $BUILD_DPATH build

GPAW_CONFIG=../siteconfig-lumi-$build.py python3 setup.py build_ext -b $BUILD_DPATH/lib -t $BUILD_DPATH/temp 2>&1 | tee $BUILD_DPATH.log

# to run on a node
# srun -p small   --nodes=1 --ntasks-per-node=1                   -t 0:30:00 ./build-lumi.sh dev cpu
# srun -p small-g --nodes=1 --ntasks-per-node=1 --gpus-per-node=1 -t 0:30:00 ./build-lumi.sh dev hip
