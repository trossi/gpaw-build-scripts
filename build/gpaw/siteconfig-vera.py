# Adapted from
# https://github.com/easybuilders/easybuild-easyconfigs/commit/ef970e49f67646eb96782b8fc7348c2de75fdb43

import os

mpicompiler = 'mpiicc'
libraries = []
library_dirs = []
include_dirs = []

# Intel MKL
libraries += ['mkl_sequential', 'mkl_core', 'mkl_rt']

# fftw
fftw = True
libraries += ['fftw3xc_intel_pic']

# scalapack
scalapack = True
libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# libxc
libraries += ['xc']

# Add all library_dirs
# include_dirs = os.getenv('CPATH').split(':')
# library_dirs = os.getenv('LD_LIBRARY_PATH').split(':')

extra_compile_args = [
    #'-w',  # disable all errors
    '-xHost',
    '-O3',
    '-ipo',
    '-funroll-all-loops',
    '-fPIC',
    '-std=c99',
    '-Wall',
    '-Wextra',
    '-DNDEBUG',
    '-fno-math-errno',
    ]

