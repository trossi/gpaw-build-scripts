compiler = 'cc -D_Float128=__float128'
mpicompiler = compiler
mpilinker = 'cc'
extra_compile_args = ['-std=c99', '-O3', '-fPIC']
define_macros += [("GPAW_ASYNC", 1)]
define_macros += [("GPAW_MPI2", 1)]

libraries = []
library_dirs = []
include_dirs = []

# Intel MKL
libraries += ['mkl_intel_lp64', 'mkl_sequential', 'mkl_core', 'mkl_rt']

# scalapack
scalapack = True
libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# fftw
fftw = True
libraries += ['fftw3']

# libxc
libraries += ['xc']
dpath = '/pdc/vol/libxc/4.3.4'
include_dirs += [f'{dpath}/include']
library_dirs += [f'{dpath}/lib']
