import os

parallel_python_interpreter = True

compiler = 'cc'
mpicompiler = compiler
mpilinker = mpicompiler
libraries = []
library_dirs = []
include_dirs = []
extra_compile_args = [
    '-std=c99',
    '-O3',
    '-march=native',
    '-mtune=native',
    '-mavx2',
    '-fopenmp',  # implies -fopenmp-simd
    '-fPIC',
    #'-Wall',
    #'-Wextra',
    '-Wno-stringop-overflow',  # suppress warnings from MPI_STATUSES_IGNORE
    #'-DNDEBUG',
    '-g',
    ]
extra_link_args = ['-fopenmp']

# fftw
fftw = True
libraries += ['fftw3']

# scalapack
scalapack = True

# libxc
libraries += ['xc']
dpath = '/appl/lumi/spack/22.08/0.18.1/opt/spack/libxc-5.1.7-c55ppsw'
include_dirs += [f'{dpath}/include']
library_dirs += [f'{dpath}/lib']
extra_link_args += [f'-Wl,-rpath,{dpath}/lib']
# runtime_library_dirs += [f'{dpath}/lib']  # This adds --enable-new-dtags -> doesn't work

define_macros += [('GPAW_ASYNC', 1)]
define_macros += [('GPAW_MPI2', 1)]
