#!/bin/bash

version=${1:-22.8.0}
build=cpu

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

echo "Building $version for $build"
echo

module purge 2> /dev/null
module load gpaw/$version

if [[ "$build" == "cpu" ]]; then
    if ! module list 2>&1 | grep -q "partition/C"; then
        echo "ERROR: partition/C not loaded"
        exit 1
    fi
elif [[ "$build" == "hip" ]]; then
    if ! module list 2>&1 | grep -q "partition/G"; then
        echo "ERROR: partition/G not loaded"
        exit 1
    fi

    which hipcc
    echo "HCC_AMDGPU_TARGET=$HCC_AMDGPU_TARGET"
fi

if module list 2>&1 | grep -q "partition/L"; then
    echo "ERROR: partition/L loaded"
    exit 1
fi

cd $version
rm -rf build build*.log

python3 -m pip install --no-deps --target build/target ase==3.22.1
GPAW_CONFIG=../siteconfig-lumi-22.8.0.py python3 -m pip install --no-deps --log build.log --target build/target .
cp build/bin.linux-x86_64-3.9/gpaw-python build/target/bin/
cp tools/gpaw build/target/bin/
sed -i '1c#!/usr/bin/env gpaw-python' build/target/bin/gpaw

# run on a node
# srun -p small --nodes=1 --ntasks-per-node=1 -t 0:30:00 ./build-lumi-22.8.0.sh
