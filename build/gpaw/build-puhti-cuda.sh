#!/bin/bash

version=$1

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

module purge
module load gpaw/$version

# To install scipy, pytest, and cupy
# python -m pip install scipy
# python -m pip install flake8 pytest pytest-xdist
# python -m pip install cupy-cuda11x


cd $version
rm -rf build

GPAW_CONFIG=../siteconfig-puhti-cuda.py python3 setup.py build_ext 2>&1 | tee build.log
