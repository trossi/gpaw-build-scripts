import os

mpicompiler = 'mpicc'
libraries = []
library_dirs = []
include_dirs = []
extra_compile_args = ['-std=c99',
                      '-O3',
                      '-fopenmp',  # implies -fopenmp-simd
                      '-march=native',
                      '-mtune=native',
                      '-fPIC',
                      #'-Wall',
                      #'-Wextra',
                      '-DNDEBUG',
                      ]
extra_link_args = ['-fopenmp']

# openblas
libraries += ['openblas']

# fftw
fftw = True
libraries += ['fftw3']

# scalapack
scalapack = True
libraries += ['scalapack']

# libxc
libraries += ['xc']
