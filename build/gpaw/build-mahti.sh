#!/bin/bash

version=$1
build=${2:-cpu}

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

echo "Building $version for $build"
echo

module purge 2> /dev/null
module load gpaw/$version-$build

# To install pytest and cupy
# python -m pip install flake8 pytest pytest-xdist
# python -m pip install cupy-cuda11x

cd $version
BUILD_DPATH=build-$build
rm -rf $BUILD_DPATH build

export CC=
GPAW_CONFIG=../siteconfig-mahti-$build.py python3 setup.py build_ext -b $BUILD_DPATH/lib -t $BUILD_DPATH/temp 2>&1 | tee $BUILD_DPATH.log
