import os
from pathlib import Path

mpicompiler = '/usr/lib64/openmpi/bin/mpicc'

fftw = False
scalapack = True
libraries = ['xc', 'blas', 'scalapack']

# Search for user libxc
dpath = Path(os.environ['GPAW_USERAPPL']) / 'libxc' / '4.3.4'
if dpath.exists():
    include_dirs += [f'{dpath}/include']
    library_dirs += [f'{dpath}/lib']
    extra_link_args += [f'-Wl,-rpath,{dpath}/lib']
