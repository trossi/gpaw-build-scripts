fftw = True
scalapack = True
libraries = ['xc', 'fftw3', 'mkl_sequential', 'mkl_core', 'mkl_rt', 'mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']
