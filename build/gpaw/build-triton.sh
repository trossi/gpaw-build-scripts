#!/bin/bash

version=$1

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

module purge
module load gpaw/$version

# To install pip and pytest
# python -m ensurepip --upgrade
# python -m pip install pip --upgrade
# python -m pip install scipy
# python -m pip install flake8 pytest pytest-xdist

cd $version
rm -rf build

GPAW_CONFIG=../siteconfig-triton.py python3 setup.py build_ext 2>&1 | tee build.log
