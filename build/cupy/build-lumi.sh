#!/bin/bash

version=$1
build=${2:-rocm533}

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

echo "Building $version for $build"
echo

module purge
module load cupy/$version-$build

module list

if ! module list 2>&1 | grep -q "partition/G"; then
    echo "ERROR: partition/G not loaded"
    exit 1
fi
if module list 2>&1 | grep -q "partition/L"; then
    echo "ERROR: partition/L loaded"
    exit 1
fi

cd $version-$build
rm -rf build build.log

python3 -m pip install --no-deps --target build/target fastrlock
python3 -m pip install --no-deps --log build.log --target build/target .

# to run on a node
# git clone --depth 1 --recurse-submodules --shallow-submodules -b v11.6.0 https://github.com/cupy/cupy.git 11.6.0-def
# srun -p small-g --nodes=1 --ntasks-per-node=1 --gpus-per-node=1 -t 0:30:00 ./build-lumi.sh 11.6.0 def
