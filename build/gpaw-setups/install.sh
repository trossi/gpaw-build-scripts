#!/bin/bash

version=0.9.20000
#version=0.9.11271

module purge
module load gpaw

mkdir $version
gpaw install-data --no-register $version --version=$version
gpaw install-data --no-register $version --basis --version=pvalence-$version
if [ $version = "0.9.11271" ]; then
    gpaw install-data --no-register $version --basis --version=NGTO-$version
fi
