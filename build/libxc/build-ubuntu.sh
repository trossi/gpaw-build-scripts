#!/bin/bash

version=$1

if [ -z "$version" ]; then
    echo "Give version"
    exit 1
fi

module purge

cd $version
autoreconf -i
./configure --enable-shared --prefix=$PWD
make -j 4
make install
make check
